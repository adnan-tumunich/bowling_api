class AddDefaultFramesToGame < ActiveRecord::Migration[5.1]
  def change
  	change_column :games, :frames_completed, :integer, :null => false, :default => 0
  end
end
