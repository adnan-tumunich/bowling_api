class AddDefaultToRolls < ActiveRecord::Migration[5.1]
  def change
  	change_column :frames, :rolls_completed, :integer, :null => false, :default => 0
  end
end
