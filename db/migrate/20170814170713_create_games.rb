class CreateGames < ActiveRecord::Migration[5.1]
  def change
    create_table :games do |t|
      t.integer :frames_completed
      t.integer :rolls_completed

      t.timestamps
    end
  end
end
