class CreateRolls < ActiveRecord::Migration[5.1]
  def change
    create_table :rolls do |t|
      t.integer :pins
      t.integer :roll_number

      t.timestamps
    end
  end
end
