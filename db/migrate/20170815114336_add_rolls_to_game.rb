class AddRollsToGame < ActiveRecord::Migration[5.1]
  def change
  	change_column :games, :rolls_completed, :integer, :null => false, :default => 0
  end
end
