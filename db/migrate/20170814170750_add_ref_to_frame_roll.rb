class AddRefToFrameRoll < ActiveRecord::Migration[5.1]
  def change
    add_reference :rolls, :game, foreign_key: true
    add_reference :rolls, :frame, foreign_key: true
    add_reference :frames, :game, foreign_key: true
  end
end
