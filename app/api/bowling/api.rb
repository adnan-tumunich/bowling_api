module Bowling
  class API < Grape::API
    version 'v1', using: :path, vendor: 'Adnan'
    format :json
    prefix :api
    content_type :json, 'application/json;charset=UTF-8'

    rescue_from :all do |e|
      errors = {errors: [{
                            status: '500',
                            detail: e.message
                          }]}
        Rails.logger.error(e.backtrace.join(""))
        Rack::Response.new(errors.to_json, 500).finish
    end

    
    resource :games do
      desc 'Return a game.'
      params do
        requires :id, type: Integer
      end
     
      get '/:id' do
        Game.find_by(id: params[:id])
      end

      params do
        requires :id, type: String
        requires :pins, type: Integer
      end
      post '/:id/bowl' do
        game = Game.find_by(id: params[:id])
        raise "Game doesn't exist" unless game 
        game.bowl(params[:pins])
        game.save
        game
      end

      post '/new' do
        game = Game.new
        game.save
        game
      end

      params do
        requires :id, type: Integer
      end
      get '/:id/total_score' do 
        game = Game.find_by(id: params[:id])
        game.total_score
      end

      params do
        requires :id, type: Integer
      end
      get '/:id/frame_scores' do 
        game = Game.find_by(id: params[:id])
        game.scores
      end
    end
  end
end
