class Roll < ApplicationRecord
  belongs_to :game
  belongs_to :frame

  def bowl(pins_remaining, pins)
    if pins > pins_remaining
      self.pins = pins_remaining
    else
      self.pins = pins
    end
    save
    self.pins
  end
end
