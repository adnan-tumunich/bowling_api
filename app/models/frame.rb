class Frame < ApplicationRecord
  belongs_to :game
  has_many :rolls
  def bowl(pins)
    self.rolls_completed += 1
    save
    if frame_number < 10
      bowl_normal(pins) 
    else
      bowl_final(pins)
    end
  end

  def strike?
    rolls.length == 1 &&
    rolls.first.pins == 10
  end

  def spare?
    !strike? &&  
    rolls.length == 2 &&
    (rolls[0].pins + rolls[1].pins == 10)
  end

  def score
    return score_final if frame_number == 10
    return score_normal
  end

  def complete?
    if frame_number < 10
      strike? || spare? || rolls_completed == 2
    else
      (rolls_completed == 2 && !can_bowl_extra_frame?) ||
      (rolls_completed == 3)
    end
  end

  private
  def bowl_normal(pins)
  	game.reload
  	roll = Roll.create(roll_number: game.rolls_completed)
    self.game.rolls << roll
    self.rolls << roll
    self.pins_remaining -= roll.bowl(pins_remaining, pins)
    self.save
  end

  def bowl_final(pins)
    reset_pins if can_bowl_extra_frame?
    bowl_normal(pins)
  end

  def score_normal
    return nil unless complete?
  

    if strike?
      next_rolls = get_next_rolls(2)
      return 10 + get_next_rolls(2).reduce(:+) if next_rolls.length == 2
    elsif spare?
      next_rolls = get_next_rolls(1)
      return 10 + get_next_rolls(1).first if next_rolls.length == 1
    else
      return rolls[0].pins + rolls[1].pins
    end
  end

  def score_final
    rolls.reduce(0) do |accum, elem|
      accum += elem.pins
    end
  end

  def reset_pins
    self.pins_remaining = 10
    self.save
  end

  def get_next_rolls(num)
    self.game.rolls.select do |roll|
      roll.roll_number > last_roll_number &&
      roll.roll_number <= (last_roll_number + num)
    end.map { |r| 
    	r.pins }
  end

  def last_roll_number
    last_roll = rolls.last
    last_roll.roll_number if last_roll
  end

  def can_bowl_extra_frame?
    frame_number == 10 && (
      strike? ||
      spare? ||
      (rolls.length == 2 && rolls[0].pins + rolls[1].pins >= 10)
    )    
  end
end
