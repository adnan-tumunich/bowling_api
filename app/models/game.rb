class Game < ApplicationRecord
  has_many :frames, -> { order(frame_number: :asc)}, dependent: :destroy
  has_many :rolls,  -> { order(roll_number: :asc)}, dependent: :destroy

  def scores
  	self.frames.map do |f|
      f.score
    end
  end

  def total_score
  	scores.sum
  end

  def current_frame
    frames.last
  end

  def bowl(pins)
    raise 'Game Over' if over?
    self.rolls_completed += 1
    if !current_frame || current_frame.complete?
      self.frames_completed += 1
      new_frame = Frame.new(
        game_id: id, 
        frame_number: frames_completed, 
        pins_remaining: 10
      )
      self.frames << new_frame
    end
    save
    current_frame.bowl(pins)
  end

  def over?
    frames_completed == 10 && frames[9].complete?
  end
end
