require 'rails_helper'

RSpec.describe Game, type: :model do
  let(:game) { Game.create! }

  describe 'play game' do
    it 'should play a perfect game and score 300' do
      12.times do
        game.bowl(10)
      end
      expect(game.total_score).to equal(300)
    end

    it 'should play a normal game' do
      20.times do
        game.bowl(3)
      end
      expect(game.total_score).to equal(60)
    end

    it 'should spare the last ball and get an extra turn' do
      21.times do |i|
        if i == 19
          game.bowl(7)
        else
          game.bowl(3)
        end
      end
      expect(game.total_score).to equal(67)
    end
  end
end
