# README

Bowling API:

Simple score calculator for bowling.

###Assumptions
1. Single player is allowed to play
2. API assumes that a new game will be started before bowling begins
3. Each game has a unique id (instead of name, as this is shorter and has a unique index built in)
4. Bowling cannot be done once the game is over and the 'Game Over' error is thrown (i.e. all the frames have been completed)

More info on terms used here and in API: [10 pin bowling](http://en.wikipedia.org/wiki/Ten-pin_bowling)
---

### API
| Path                          | Method | Purpose                                       |
|-------------------------------|--------|-----------------------------------------------|
| api/v1/games/new              |  POST  | Create a new game of bowling                  |
| api/v1/games/:id              |  GET   | View bowling game with the given id           |
| api/v1/games/:id/bowl         |  POST  | Bowl a single bowl with pins (integer) as data|
| api/v1/games/:id/frame_scores |  GET   | Gets an array of frame scores                 |
| api/v1/games/:id/total_score  |  GET   | Gets the total score for the current game     |